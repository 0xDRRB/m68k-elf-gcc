# GCC toolchain builder for the Motorola m68k CPU family

This is a fork of [m68k-elf-gcc scripts](https://github.com/kentosama/m68k-elf-gcc) with all PR and patchs applied, various fixes and modified to be cross-platform (works on GNU/Linux, FreeBSD, OpenBSD and NetBSD). Since the PRs have been pending for ~3 years, I decided to make my fork here. You can view the original README in the `README.md.orig` file.

This is a set of bash scripts for build gcc toolchain on Unix environment for the Motorola 68000 family (m68k).

## Build the toolchain

```
$ cd ~/somewhere
$ git clone https://github.com/kentosama/m68k-elf-gcc.git
$ cd m68k-elf-gcc
$ ./build-toolchain.sh
[...]
m68k compiler build successfully completed. Creating archive...

done.
You can now copy
/home/denis/HUB/m68k-elf-gcc/gcc-m68k-6.3.0-FreeBSD_amd64
or unarchive 'gcc-m68k-6.3.0-FreeBSD_amd64.tar.xz' wherever you want.
```

For build the toolchain with the newlib, use `--with-newlib` argument:

```
$ ./build-toolchain.sh --with-newlib
```

For build the toolchain with specific processors of the Motrola 68000 family, use `--with-cpu` argument:

```
$ ./build-toolchain.sh --with-cpu=68000,68030...
```

For change the program prefix, use `--program-prefix` argument:

```
$ ./build-toolchain.sh --program-prefix=sega-genesis-
```

## Install

Once the Motorola 68000 toolchain was successful built, you can move or copy the toolchain directory in `/opt`, `/usr/local` or anywhere else and add it to your `PATH`. A toolchain tarball is also created at the root of the repository.

You can check that `m68k-elf-gcc` is working properly:

```
$ ./somewhere/bin/m68k-elf-gcc -v
```

The result should display something like this (here with FreeBSD 13.1):

```
Using built-in specs.
COLLECT_GCC=./gcc-m68k-6.3.0-FreeBSD_amd64/bin/m68k-elf-gcc
COLLECT_LTO_WRAPPER=/usr/home/denis/HUB/m68k-elf-gcc/gcc-m68k-6.3.0-FreeBSD_amd64/bin/../libexec/gcc/m68k-elf/6.3.0/lto-wrapper
Target: m68k-elf
Configured with: ../../source/gcc-6.3.0/configure --prefix=/home/denis/HUB/m68k-elf-gcc/gcc-m68k-6.3.0-FreeBSD_amd64 --build=amd64-portbld-freebsd13.0 --host=amd64-portbld-freebsd13.0 --target=m68k-elf --program-prefix=m68k-elf- --enable-languages=c --enable-obsolete --enable-lto --disable-threads --disable-libmudflap --disable-libgomp --disable-nls --disable-werror --disable-libssp --disable-shared --disable-multilib --disable-libgcj --disable-libstdcxx --disable-gcov --without-headers --without-included-gettext --with-cpu=m68000
Thread model: single
gcc version 6.3.0 (GCC)
```

You can display help specific to the compilation target with:

```
$ ./m68k-toolchain/bin/m68k-elf-gcc --target-help
The following options are target specific:
  -m5200                      Generate code for a 520X.  Same as -mcpu=.
[...]
  -m68000                     Generate code for a 68000.  Same as -mcpu=.
  -m68010                     Generate code for a 68010.  Same as -mcpu=.
  -m68020                     Generate code for a 68020.  Same as -mcpu=.
  -m68020-40                  Generate code for a 68040, without any new
                              instructions.
  -m68020-60                  Generate code for a 68060, without any new
                              instructions.
  -m68030                     Generate code for a 68030.  Same as -mcpu=.
  -m68040                     Generate code for a 68040.  Same as -mcpu=.
  -m68060                     Generate code for a 68060.  Same as -mcpu=.
[...]
  -mcpu32                     Generate code for a cpu32.  Same as -mcpu=.
  -mcpu=                      Specify the target CPU.
  -mdiv                       Use hardware division instructions on ColdFire.
  -mfidoa                     Generate code for a Fido A.
[...]
  Known M68K ISAs (for use with the -march= option):
    68000 68010 68020 68030 68040 68060 cpu32 isaa isaaplus isab isac

  Known M68K CPUs (for use with the -mcpu= option):
    51 51ac 51ag 51cn 51em 51je 51jf 51jg 51jm 51mm 51qe 51qm 5202 5204 5206
    5206e 5207 5208 5210a 5211 5211a 5212 5213 5214 5216 5221x 52221 52223 52230
    52231 52232 52233 52234 52235 5224 5225 52252 52254 52255 52256 52258 52259
    52274 52277 5232 5233 5234 5235 523x 5249 5250 5253 5270 5271 5272 5274 5275
    5280 5281 5282 528x 53011 53012 53013 53014 53015 53016 53017 5307 5327 5328
    5329 532x 5372 5373 537x 5407 54410 54415 54416 54417 54418 54450 54451
    54452 54453 54454 54455 5470 5471 5472 5473 5474 5475 547x 5480 5481 5482
    5483 5484 5485 548x 68000 68010 68020 68030 68040 68060 68302 68332 cpu32
    fidoa

  Known M68K microarchitectures (for use with the -mtune= option):
    68000 68010 68020 68020-40 68020-60 68030 68040 68060 cfv1 cfv2 cfv3 cfv4
    cfv4e cpu32
Assembler options
=================

Use "-Wa,OPTION" to pass "OPTION" to the assembler.

-march=<arch>           set architecture
-mcpu=<cpu>             set cpu [default 68020]
-m[no-]68851            enable/disable  m68k architecture extension
-m[no-]68881            enable/disable  m68k architecture extension
[...]
```
