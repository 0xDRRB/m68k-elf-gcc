#!/usr/bin/env bash

###################################################################
#Script Name :   build-gcc
#Description :   build gcc for the Motorola 68000 toolchain
#Date        :   samedi, 4 avril 2020
#Args        :   Welcome to the next level!
#Author      :   Jacques Belosoukinski (kentosama)
#Email       :   kentosama@genku.net
#
#Update      :   by Denis Bodor (0xDRRB) 05/22/2023
##################################################################

source colorprint.sh

if [ ${GCCVER} == "11" ]; then
    VERSION="11.3.0"
    ARCHIVE="gcc-${VERSION}.tar.xz"
    URL="https://gcc.gnu.org/pub/gcc/releases/gcc-${VERSION}/${ARCHIVE}"
    SHA512SUM="f0be5ad705c73b84477128a69c047f57dd47002f375eb60e1e842e08cf2009a509e92152bca345823926d550b7395ae6d4de7db51d1ee371c2dc37313881fca7"
    DIR="gcc-${VERSION}"
elif [ ${GCCVER} == "10" ]; then
    VERSION="10.3.0"
    ARCHIVE="gcc-${VERSION}.tar.xz"
    URL="https://gcc.gnu.org/pub/gcc/releases/gcc-${VERSION}/${ARCHIVE}"
    SHA512SUM="2b2dd7453d48a398c29eaebd1422b70341001b8c90a62aee51e83344e7fdd8a8e45f82a4a9165bd7edc76dada912c932f4b6632c5636760fec4c5d7e402b3f86"
    DIR="gcc-${VERSION}"
elif [ ${GCCVER} == "6" ]; then
    VERSION="6.3.0"
    ARCHIVE="gcc-${VERSION}.tar.bz2"
    URL="https://gcc.gnu.org/pub/gcc/releases/gcc-${VERSION}/${ARCHIVE}"
    SHA512SUM="234dd9b1bdc9a9c6e352216a7ef4ccadc6c07f156006a59759c5e0e6a69f0abcdc14630eff11e3826dd6ba5933a8faa43043f3d1d62df6bd5ab1e82862f9bf78"
    DIR="gcc-${VERSION}"
else
    printerror "Unsupported version. Abord!"
    exit 1
fi

# Check if user is root
if [ ${EUID} == 0 ]; then
    echo "Please don't run this script as root"
    exit
fi

# Create build folder
mkdir -p ${BUILD_DIR}/${DIR}

cd ${DOWNLOAD_DIR}

# Download gcc if is needed
if ! [ -f "${ARCHIVE}" ]; then
    wget ${URL}
fi

# Extract gcc archive if is needed
if ! [ -d "${SRC_DIR}/${DIR}" ]; then
    if [ $(${SUMCMD} ${ARCHIVE} | awk '{print $1}') != ${SHA512SUM} ]; then
        echo "SHA512SUM verification of ${ARCHIVE} failed!"
        exit 1
    else
        printstep "Extracting GCC archive"
        tar jxf ${ARCHIVE} -C ${SRC_DIR}

	if [ ${GCCVER} == "6" ]; then
            # Apply patch for ubsan.c at 1474:
	    # || xloc.file == '\0' || xloc.file[0] == '\xff' to
	    # || xloc.file[0] == '\0' || xloc.file[0] == '\xff'
	    patch -t ${SRC_DIR}/${DIR}/gcc/ubsan.c < ${ROOT_DIR}/patch/ubsan-fix-check-empty-string.patch
	fi

    fi
fi

cd ${SRC_DIR}/${DIR}

echo ${PWD}

# Download prerequisites
./contrib/download_prerequisites

cd ${BUILD_DIR}/${DIR}

# Configure before build
../../source/${DIR}/configure   --prefix=${INSTALL_DIR}             \
                                --build=${BUILD_MACH}               \
                                --host=${HOST_MACH}                 \
                                --target=${TARGET}                  \
                                --program-prefix=${PROGRAM_PREFIX}  \
                                --enable-languages=c                \
                                --enable-obsolete                   \
                                --enable-lto                        \
                                --disable-threads                   \
                                --disable-libmudflap                \
                                --disable-libgomp                   \
                                --disable-nls                       \
                                --disable-werror                    \
                                --disable-libssp                    \
                                --disable-shared                    \
                                --disable-multilib                  \
                                --disable-libgcj                    \
                                --disable-libstdcxx                 \
                                --disable-gcov                      \
                                --without-headers                   \
                                --without-included-gettext          \
                                --with-cpu=${WITH_CPU}              \
                                ${WITH_NEWLIB}


# build and install gcc
${MAKE} -j${NUM_PROC} 2<&1 | tee build.log

# Install
if [ $? -eq 0 ]; then
    ${MAKE} install
    ${MAKE} -j${NUM_PROC} all-target-libgcc
    ${MAKE} install-target-libgcc
fi


