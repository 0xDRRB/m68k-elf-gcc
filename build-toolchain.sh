#!/usr/bin/env bash

###################################################################
#Script Name :   build-toolchain
#Description :   build toolchain for the Motorola 68000
#Date        :   samedi, 4 avril 2020
#Args        :   Welcome to the next level!
#Author      :   Jacques Belosoukinski (kentosama)
#Email       :   kentosama@genku.net
#
#Update      :   by Denis Bodor (0xDRRB) 05/22/2023
###################################################################

BUILD_BINUTILS="yes"
BUILD_GCC_STAGE_1="yes"
BUILD_GCC_STAGE_2="no"
BUILD_NEWLIB="no"
BUILD_GCC_VERSION=${1:-"6"}
CPU="m68000"
PREFIX="m68k-elf-"
HOSTSYSTEM=$(uname -s)
HOSTARCH=$(uname -m)

source colorprint.sh

# Check if user is root
if [ ${EUID} == 0 ]; then
    echo "Please don't run this script as root!"
    exit 1
fi

if [ ${BUILD_GCC_VERSION} == "11" ]; then
    INSTALL_BASEDIR="gcc-m68k-11.3.0-${HOSTSYSTEM}_${HOSTARCH}"
elif [ ${BUILD_GCC_VERSION} == "10" ]; then
    INSTALL_BASEDIR="gcc-m68k-10.3.0-${HOSTSYSTEM}_${HOSTARCH}"
elif [ ${BUILD_GCC_VERSION} == "6" ]; then
    INSTALL_BASEDIR="gcc-m68k-6.3.0-${HOSTSYSTEM}_${HOSTARCH}"
else
    printerror "Unsupported version. Abord!"
    exit 1
fi

# Check all args
i=1

for arg do
    if [[ "$arg" == "--with-newlib" ]]; then
        BUILD_NEWLIB="yes"
        BUILD_GCC_STAGE_2="yes"
        export WITH_NEWLIB="--with-newlib"
    elif [[ "$arg" == "--with-cpu=" ]]; then
        CPU = ${i}
    elif [[ "$arg" == "--program-prefix=" ]]; then
        PREFIX = ${i}
    fi

    i=$((i + 1))
done

# Export
if [ "${HOSTSYSTEM}" = 'FreeBSD' ] || [ "${HOSTSYSTEM}" = 'OpenBSD' ] || [ "${HOSTSYSTEM}" = 'NetBSD' ]; then
    export MAKE="gmake"
    export NUM_PROC=$(sysctl -n hw.ncpu)
    if [ "${HOSTSYSTEM}" = 'FreeBSD' ]; then
        export SUMCMD="sha512sum"
    fi
    if [ "${HOSTSYSTEM}" = 'OpenBSD' ]; then
        export SUMCMD="cksum -r -a SHA512"
    fi
    if [ "${HOSTSYSTEM}" = 'NetBSD' ]; then
        export SUMCMD="cksum -n -a SHA512"
    fi
else
    # Presume it's GNU/Linux
    export MAKE="make"
    export NUM_PROC=$(nproc)
    export SUMCMD="sha512sum"
fi
export ARCH=$(uname -m)
export TARGET="m68k-elf"
export BUILD_MACH="$MACHTYPE"
export HOST_MACH="$MACHTYPE"
export PROGRAM_PREFIX=${PREFIX}
#export INSTALL_DIR="${PWD}/gcc-m68k-6.3.0-${HOSTSYSTEM}_${HOSTARCH}"
export INSTALL_DIR="${PWD}/${INSTALL_BASEDIR}"
export DOWNLOAD_DIR="${PWD}/download"
export ROOT_DIR="${PWD}"
export BUILD_DIR="${ROOT_DIR}/build"
export SRC_DIR="${ROOT_DIR}/source"
export WITH_CPU=${CPU}
export GCCVER=${BUILD_GCC_VERSION}

# Create main folders in the root dir
mkdir -p ${INSTALL_DIR}
mkdir -p ${BUILD_DIR}
mkdir -p ${SRC_DIR}
mkdir -p ${DOWNLOAD_DIR}

export PATH=$INSTALL_DIR/bin:$PATH

# Build binutils
if [ ${BUILD_BINUTILS} == "yes" ]; then
    printtitle "Building binutils"
    ./build-binutils.sh
    if [ $? -ne 0 ]; then
        echo "Failed to build binutils, please check build.log"
        exit 1
    fi
fi

# Build GCC stage 1
if [ ${BUILD_GCC_STAGE_1} == "yes" ]; then
    printtitle "Building GCC stage 1"
    ./build-gcc.sh
    if [ $? -ne 0 ]; then
        echo "Failed to build gcc stage 1, please check build.log"
        exit
    fi
fi

# Build newlib
if [ ${BUILD_NEWLIB} == "yes" ]; then
    printtitle "Building newlib"
    ./build-newlib.sh
    if [ $? -ne 0 ]; then
        echo "Failed to build newlib, please check build.log"
        exit
    else
        # Build GCC stage 2 (with newlib)
        if [ ${BUILD_GCC_STAGE_2} == "yes" ]; then
            printtitle "Building GCC stage 2 (with newlib)"
            ./build-gcc.sh
            if [ $? -ne 0 ]; then
                echo "Failed to build gcc stage 2, please check build.log"
                exit
            fi
        fi
    fi
fi

printtitle "Building successfully!"

echo "m68k compiler build successfully completed"
printstep "Creating archive"

# Build archive
tar cJf ${INSTALL_BASEDIR}.tar.xz ${INSTALL_BASEDIR}

echo -e "\ndone.\nYou can now copy ${INSTALL_DIR} or unarchive '${INSTALL_BASEDIR}.tar.xz' wherever you want."
