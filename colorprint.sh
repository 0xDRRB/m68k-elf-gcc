COLOR_BLACK="\e[0;30m"
COLOR_RED="\e[0;31m"
COLOR_GREEN="\e[0;32m"
COLOR_YELLOW="\e[0;33m"
COLOR_BLUE="\e[0;34m"
COLOR_MANGENTA="\e[0;35m"
COLOR_CYAN="\e[0;36m"
COLOR_WHITE="\e[0;37m"
COLOR_BBLACK="\e[1;30m"
COLOR_BRED="\e[1;31m"
COLOR_BGREEN="\e[1;32m"
COLOR_BYELLOW="\e[1;33m"
COLOR_BBLUE="\e[1;34m"
COLOR_BMANGENTA="\e[1;35m"
COLOR_BCYAN="\e[1;36m"
COLOR_BWHITE="\e[1;37m"
COLOR_RST="\e[0m"

printtitle() {
    echo -e "${COLOR_BGREEN}**************************************************************************${COLOR_RST}"
    for ((i=1;i<=$(((74-${#1})/2));i++))
    do
        echo -n " "
    done
    echo -e "${COLOR_BYELLOW}$1${COLOR_RST}"
    echo -e "${COLOR_BGREEN}**************************************************************************${COLOR_RST}"
}

printstep() {
    echo -e "    ${COLOR_BGREEN}$1...${COLOR_RST}"
}

printerror() {
    echo -e "${COLOR_BRED}$1${COLOR_RST}"
}
